#include "Surface.h"
#include "GameComponentVisitor.h"
#include "BoundingBox.h"

/**
 * Constructor.
 * \param w Width of the surface
 * \param h Height of the surface
 * \param pos Position of the surface
 */
CSurface::CSurface(float w, float h, glm::vec3 pos) : CRectangle(pos, glm::vec3(w, h, 2.0f) )
{
}


/**
 * Destructor.
 */
CSurface::~CSurface()
{
}


/**
 * Accept a visitor.
 * \param visitor Visitor to accept.
 */
void CSurface::AcceptVisitor(CGameComponentVisitor * visitor)
{
	visitor->VisitSurface(this);
}
/**
* Draw this object
* \param view View matrix of the camera.
* \param perspective Perspective matrix of the camera.
*/
void CSurface::Draw(glm::mat4 view, glm::mat4 perspective)
{
	CRectangle::Draw(view, perspective);
	glm::mat4 mvp = perspective * view * GetTransformMatrix();
	CBoundingBox * box = GetBoundingBox();
	//box->DrawBox(mvp);
}
