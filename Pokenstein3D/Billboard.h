/**
 * \file Billboard.h
 *
 * \author Samantha Oldenburg
 *
 * A rectangle that always faces towards the camera. 
 */

#pragma once
#include "Rectangle.h"

/**
 * A rectangle that always faces towards the camera. 
 *
 * The actual orienting of the billboard occurs within
 * the vertex shader.
 */
class CBillboard :
	public CRectangle
{
public:
	CBillboard(float w, float h, glm::vec3 pos);
	virtual ~CBillboard();


	virtual void Initialize() override;
	virtual void Draw(glm::mat4 view, glm::mat4 perspective);

protected:
	/** Get the reference to the ModelViewID in the shader 
	 * \returns the reference to the ID*/
	GLuint & GetModelViewID() { return mModelViewID; }

	/** Get the reference to the ModelViewID in the shader
	* \returns the reference to the ID*/
	GLuint & GetProjectionID() { return mProjectionID; }
	
	

private:
	/// ID of a matrix that is the  View matrix * Model matrix
	GLuint mModelViewID;
	/// ID of a matrix that is the projection matrix of the camera.
	GLuint mProjectionID;
	

};

