/**
 * \file Player.h
 *
 * \author Samantha Oldenburg
 *
 * And invisible player that the camera follows.
 */

#pragma once
#include "PhysicalGameComponent.h"
/**
 * An invisible player that the camera follows.
 */
class CPlayer :
	public CPhysicalGameComponent
{
public:
	CPlayer();
	virtual ~CPlayer();
	void Move(glm::vec3 translate, glm::vec3 forward);
};

