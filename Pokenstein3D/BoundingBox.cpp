/**
 * \file BoundingBox.cpp
 *
 * \author Samantha Oldenburg
 */

#include "BoundingBox.h"
#include "PhysicalGameComponent.h"
#include "RenderingManager.h"

/// Vertices to use to draw this box for visual debugging.
const glm::vec3 CubeVertices[36] = {
glm::vec3(-1.0, -1.0f, -1.0f), // triangle 1 : begin
glm::vec3(-1.0f, -1.0f, 1.0f),
glm::vec3(-1.0f, 1.0f, 1.0f),
glm::vec3(1.0f, 1.0f, -1.0f),
glm::vec3(-1.0f, -1.0f, -1.0f),
glm::vec3(-1.0f, 1.0f, -1.0f),
glm::vec3(1.0f, -1.0f, 1.0f),
glm::vec3(-1.0f, -1.0f, -1.0f),
glm::vec3(1.0f, -1.0f, -1.0f),
glm::vec3(1.0f, 1.0f, -1.0f),
glm::vec3(1.0f, -1.0f, -1.0f),
glm::vec3(-1.0f, -1.0f, -1.0f),
glm::vec3(-1.0f, -1.0f, -1.0f),
glm::vec3(-1.0f, 1.0f, 1.0f),
glm::vec3(-1.0f, 1.0f, -1.0f),
glm::vec3(1.0f, -1.0f, 1.0f),
glm::vec3(-1.0f, -1.0f, 1.0f),
glm::vec3(-1.0f, -1.0f, -1.0f),
glm::vec3(-1.0f, 1.0f, 1.0f),
glm::vec3(-1.0f, -1.0f, 1.0f),
glm::vec3(1.0f, -1.0f, 1.0f),
glm::vec3(1.0f, 1.0f, 1.0f),
glm::vec3(1.0f, -1.0f, -1.0f),
glm::vec3(1.0f, 1.0f, -1.0f),
glm::vec3(1.0f, -1.0f, -1.0f),
glm::vec3(1.0f, 1.0f, 1.0f),
glm::vec3(1.0f, -1.0f, 1.0f),
glm::vec3(1.0f, 1.0f, 1.0f),
glm::vec3(1.0f, 1.0f, -1.0f),
glm::vec3(-1.0f, 1.0f, -1.0f),
glm::vec3(1.0f, 1.0f, 1.0f),
glm::vec3(-1.0f, 1.0f, -1.0f),
glm::vec3(-1.0f, 1.0f, 1.0f),
glm::vec3(1.0f, 1.0f, 1.0f),
glm::vec3(-1.0f, 1.0f, 1.0f),
glm::vec3(1.0f, -1.0f, 1.0f) };

/// Vertex shader to use for visual debugging
const std::string VertexShader("Shaders/SimpleTransform.vertexshader");
/// Fragment shader to use for visual debugging.
const std::string FragmentShader("Shaders/SingleColor.fragmentshader");

/**
 * Constructor.
 * \param component The component that owns this.
 * \param scale The size of the bouding box (the size of the parent component most likely).
 */
CBoundingBox::CBoundingBox(CPhysicalGameComponent * component, glm::vec3 scale) : mComponent(component)
{

	scale = scale * 0.5f;
	
	mMaxPos = glm::vec3(1.0f, 1.0f, 1.0f) * scale;
	mMinPos = glm::vec3(-1.0f, -1.0f, -1.0f) * scale;

	for (glm::vec3 vertex : CubeVertices)
	{
		mVertices.push_back(vertex * scale);
	}
	Initialize();
}


/**
 * Destrctor.
 */
CBoundingBox::~CBoundingBox()
{
}


/**
 * Initialize the bounding box as a drawable object.
 */
void CBoundingBox::Initialize()
{
	mProgramID = CRenderingManager::LoadShaders(VertexShader.c_str(), FragmentShader.c_str());
	glGenBuffers(1, &mVertexbuffer);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);

	glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(glm::vec3), &mVertices[0], GL_STATIC_DRAW);

	
	mMatrixID = glGetUniformLocation(mProgramID, "MVP");
	mColorID = glGetUniformLocation(mProgramID, "colorI");


	glUseProgram(mProgramID);
	mVertexPositionID = glGetAttribLocation(mProgramID, "vertexPosition_modelspace");
}

/**
 * Determines if a point is in the bounding box
 * \param point Location of the point.
 * \returns True if the point is in the bounding box.
 */
bool CBoundingBox::HitTest(glm::vec3 point)
{
	glm::mat4 transform = mComponent->GetTransformMatrix();
	glm::vec4 currentPosMax = transform * glm::vec4(mMaxPos, 1.0f);

	glm::vec4 currentPosMin = transform * glm::vec4(mMinPos, 1.0f);
	
	//
	// If a point is  between the maximum and minimum coordinates the box
	// has in each dimension, the point is in the box.
	//
	return (
		point.x >= currentPosMin.x &&
		point.x <= currentPosMax.x &&
		point.y >= currentPosMin.y &&
		point.y <= currentPosMax.y &&
		point.z >= currentPosMin.z &&
		point.z <= currentPosMax.z);
}



/**
 * Draws a cube where the box will be. Used for visual debugging debugging.
 * \param mvp The matrix used by this boxes drawable game component 
 * to be drawn.
 */
void CBoundingBox::DrawBox(glm::mat4 mvp)
{

	glm::mat4 MVP = mvp;

	glUseProgram(mProgramID);
	glUniformMatrix4fv(mMatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniform4f(mColorID, mColor.r, mColor.g, mColor.b, mColor.a);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);
	glVertexAttribPointer(
		mVertexPositionID,
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);

	glDrawArrays(GL_TRIANGLES, (GLint)0, (GLsizei)mVertices.size()); // 3 indices starting at 0 -> 1 triangle

	int x = 1;
	glDisableVertexAttribArray(0);




}
