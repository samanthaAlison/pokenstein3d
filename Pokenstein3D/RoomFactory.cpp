/**
 * \file RoomFactory.cpp
 *
 * \author Samantha Oldenburg
 */

#include "RoomFactory.h"
#include "DrawableGameComponent.h"
#include "Surface.h"

/**
 * Constructor. 
 */
CRoomFactory::CRoomFactory()
{
}


/**
 * Destructor.
 */
CRoomFactory::~CRoomFactory()
{
}


/**
 * Creates a room. 
 * \returns Shared pointer to the new room created.
 */
std::shared_ptr<CDrawableGameComponent> CRoomFactory::Create()
{
	std::shared_ptr<CDrawableGameComponent > room = std::make_shared<CDrawableGameComponent>(glm::vec3(1.0f, 1.0f, 1.0f));
	

	return room;


}

/**
 * Creates a room.
 * \param w Width of the room.
 * \param h Height of the room.
 * \param d Depth of the room
 * \returns Shared pointer to the new room created.
 */
std::shared_ptr<CDrawableGameComponent> CRoomFactory::Create(float w, float h, float d)
{
	std::shared_ptr<CDrawableGameComponent > room = std::make_shared<CDrawableGameComponent>(glm::vec3(w, h, d));


	//
	// Floor. Center of the floor will be the (0, 0) point of the room.
	//



	std::shared_ptr<CSurface> floor = std::make_shared<CSurface>(w, h, glm::vec3(0.0f, 0.0f, 0.0f));
	floor->SetColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	floor->SetRotation(glm::quat(glm::sqrt(0.5f), -glm::sqrt(0.5f), 0.0f, 0.0f));
	floor->UpdateMatrix();
	floor->Initialize();

	//
	// Left wall.
	//

	std::shared_ptr<CSurface> leftWall = std::make_shared<CSurface>(w, h, glm::vec3(-w / 2.0f, -h / 2.0f, 0.0f));
	leftWall->SetColor(glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	leftWall->SetRotation(glm::quat(glm::sqrt(0.5f), 0.0f, glm::sqrt(0.5f), 0.0f));
	leftWall->UpdateMatrix();
	leftWall->Initialize();


	//
	// Front wall.
	//
	
	std::shared_ptr<CSurface> frontWall = std::make_shared<CSurface>(w, h, glm::vec3(0.0f, -h / 2.0f, d / 2.0f));
	frontWall->SetColor(glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
	//frontWall->Rotate(-3.14f, glm::vec3(0, 1, 0));

	frontWall->Initialize();

	room->AddChild(frontWall);
	room->AddChild(leftWall);
	room->AddChild(floor);

	return room;
}
