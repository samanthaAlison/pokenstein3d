/**
 * \file Charmander.cpp
 *
 * \author Samantha Oldenburg
 */


#include "Charmander.h"

/// The file name of the charmander image.
const char * CharmanderFileName("images/charmanderpng.data");
/// Width of the charmander image.
const int CharmanderWidth(100);
/// Height of the charmander image.
const int CharmanderHeight(113);



/**
 * Constructor.
 * \param pos Position of item
 */
CCharmander::CCharmander(glm::vec3 pos) : CPokemon(pos)
{

}

/** Destructor */
CCharmander::~CCharmander()
{
}


/** Load the texture for this item */
void CCharmander::LoadTexture()
{
	SetTexture(CharmanderFileName, CharmanderWidth, CharmanderHeight);

}
