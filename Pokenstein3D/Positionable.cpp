/**
 * \file Positionable.cpp
 *
 * \author Samantha Oldenburg
 */

#include "Positionable.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/norm.hpp>
/// Represents the up vector
const glm::vec3 Up(glm::vec3(0.0f, 1.0f, 0.0f));


/**
 * Constructor.
 */
CPositionable::CPositionable()
{
	mTransformMatrix = glm::mat4();
	glm::decompose(mTransformMatrix, mScale, mRotation, mPosition, mSkew, mPerspective);
	mRotation = glm::quat(0, 0, 0, 1);
}


/**
 * Constructor.
 * \param p Position vector.
 * \param r Rotation vector.
 * \param s Scale vector.
 */
CPositionable::CPositionable(glm::vec3 p, glm::quat r, glm::vec3 s) : mPosition(p), mRotation(r), mScale(s)
{
	UpdateMatrix();
}


/**
 * Constructor.
 * \param matrix Tranform matrix to use for this object.
 */
CPositionable::CPositionable(glm::mat4 matrix) : mTransformMatrix(matrix)
{
	glm::decompose(mTransformMatrix, mScale, mRotation, mPosition, mSkew, mPerspective);
	
}


/**
 * Destructor.
 */
CPositionable::~CPositionable()
{
}


/**
 * Updates the tranformation matrix based on the position, scale, and rotation.
 */
void CPositionable::UpdateMatrix()
{

	glm::mat4 posMat = glm::translate(glm::mat4(), mPosition);
	glm::mat4 roMat = glm::mat4_cast(mRotation);
	glm::mat4 scaMat = glm::scale(glm::mat4(), mScale);
	mTransformMatrix = posMat * roMat * scaMat;
}


/**
 * Gets the position of the object
 * \returns The object's position.
 */
glm::vec3 CPositionable::GetPosition()
{
	return mPosition;
}


/**
 * Sets the position of the object. Updates transform matrix.
 * \param pos New position of the object.
 */
void CPositionable::SetPosition(glm::vec3 pos)
{
	mPosition = pos;
	UpdateMatrix();
}


/**
 * Applies a rotation to the object.
 * \param rotation The rotation to apply, as a vector of rotations in radians.
 */
void CPositionable::Rotate(glm::vec3 rotation)
{

	mRotation = glm::quat(rotation) * mRotation;
	UpdateMatrix();
}


/**
 * Applies a rotation to the object.
 * \param theta Magnitude of the rotation.
 * \param axis Axis of rotation.
 */
void CPositionable::Rotate(float theta, glm::vec3 axis)
{

	glm::quat rotation = glm::angleAxis(theta, axis);
	mRotation = rotation * mRotation;
}


/**
 * Makes the object face towards a point in spherical coordinates. 
 * \param h Horizontal angle to set.
 * \param v Vertical angle to set.
 */
void CPositionable::FaceAt(double h, double v)
{
	mHorizontalAngle = h;
	mVerticalAngle = v;
	UpdateMatrix();
}


/**
 * Set the rotation quaternion of the object.
 * \param q New rotation of the object.
 */
void CPositionable::SetRotation(glm::quat q)
{
	mRotation = q; 
}



/**
 * Gets the rotation as a vector.
 * \returns The rotation quaternion applied to the vector that 
 * represents then forward vector.
 */
glm::vec3 CPositionable::GetRotationVector()
{

	return mRotation * glm::vec3(0.0f, 0.0f, 1.0f);
}




/**
 * Move the object relative to it's current position.
 * \param translate Vector to add to the  objects roations vector.
 */
void CPositionable::Translate(glm::vec3 translate)
{
	mPosition +=  translate;
	UpdateMatrix();
}

