/**
 * \file Bulbasaur.cpp
 *
 * \author Samantha Oldenburg
 */

#include "Bulbasaur.h"
/// Image file of the bulbasaur
const char * BulbasaurFileName("images/bulbasaurpng.data");
/// Width of bulbasaur image.
const int BulbasaurWidth(100);
/// Height of bulbasaur image.
const int BulbasaurHeight(93);



/**
 * Constructor.
 * \param pos Position of the bulbasaur
 */
CBulbasaur::CBulbasaur(glm::vec3 pos) : CPokemon(pos)
{
	
}


/**
 * Destructor 
 */
CBulbasaur::~CBulbasaur()
{
}

/**
 * Loads the bulbasaur image as a texture.
 */
void CBulbasaur::LoadTexture()
{
	SetTexture(BulbasaurFileName, BulbasaurWidth, BulbasaurHeight);

}
