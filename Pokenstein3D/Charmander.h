/**
 * \file Charmander.h
 *
 * \author Samantha Oldenburg
 *
 * A type of pokemon.
 */

#pragma once
#include "Pokemon.h"
/**
 * A type of pokemon.
 */
class CCharmander :
	public CPokemon
{
public:
	CCharmander(glm::vec3 pos);
	virtual ~CCharmander();

	virtual void LoadTexture() override;
};

