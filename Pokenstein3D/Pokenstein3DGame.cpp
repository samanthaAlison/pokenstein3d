#include "Pokenstein3DGame.h"
#include "GameComponent.h"
#include  <stdio.h>
#include <stdlib.h>
#include "RenderingManager.h"
#include <iostream>
#include "Pokemon.h"
#include <time.h>
#include "Blastoise.h"
#include "Charmander.h"
#include "Bulbasaur.h"
#include "DrawableGameComponent.h"
#include "Pokeball.h"

#include <random>

/// Time before a pokeball can be thrown again in seconds.
const double PokeballCooldown(1.0);

/**
 * Constructor. 
 */
CPokenstein3DGame::CPokenstein3DGame()
{


}

/**
 * Destructor. 
 */
CPokenstein3DGame::~CPokenstein3DGame()
{
}


/**
 * Start the game.
 * \returns 0 if the game ends without crashing. -1 if it fails.
 */
int CPokenstein3DGame::Start()
{
	srand((unsigned int)time(nullptr));
	CRenderingManager * renderingManager = CRenderingManager::Instance();
	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
	}
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 
	glEnable(GL_TEXTURE_2D);
	
	
	// Open a window and create its OpenGL contex	
	mWindow = glfwCreateWindow(1024, 768, "Tutorial 01", NULL, NULL);
	if (mWindow == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}


	glfwMakeContextCurrent(mWindow);
	// Initialize GLEW
	if (glfwGetCurrentContext() == NULL)
	{
		int x = 0;
	}
	glewExperimental = true; // Needed in core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}
	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	renderingManager->Initialize(mWindow);
	
	for (int i = 0; i < 4; i++)
	{
		SpawnPokemon();
	}

	mPlayer = std::make_shared<CPlayer>();
	srand((unsigned int)time(nullptr));
	do {
		
		Update();
		
		renderingManager->Draw();
		if (mFirstCycle)
		{
			mPlayer->SetPosition(glm::vec3(0.0, -2.0f, 0.0f));
			CRenderingManager::Instance()->MoveCamera(mPlayer->GetPosition() + glm::vec3(0.0f, -2.0f, 0.0f));



			renderingManager->Load();
			mFirstCycle = false;
		}
	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(mWindow) == 0);
	glDeleteVertexArrays(1, &vertexArrayID);

	return 0;
	
}





/**
 * Collects input from the keyboard. 
 * Called during each update call.  
 * \param elapsed Time since last update call.
 */
void CPokenstein3DGame::GetControls(float elapsed)
{
	
	// Move forward
	if (glfwGetKey(mWindow, GLFW_KEY_UP) == GLFW_PRESS) {
		MovePlayer(glm::vec2(0.75f, 0.0f)  * elapsed);
	}
	// Move backward
	if (glfwGetKey(mWindow, GLFW_KEY_DOWN) == GLFW_PRESS) {
		MovePlayer(glm::vec2(-0.75f, 0.0f)  * elapsed);
	}
	// Strafe right
	if (glfwGetKey(mWindow, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		MovePlayer(glm::vec2(0.0f, 0.75f)  * elapsed);
	}
	// Strafe left
	if (glfwGetKey(mWindow, GLFW_KEY_LEFT) == GLFW_PRESS) {
		MovePlayer(glm::vec2(0.0f, -0.75f)  * elapsed);
	}

	// Look up
	if (glfwGetKey(mWindow, GLFW_KEY_W) == GLFW_PRESS) 
	{
		RotateCamera(0, 3.14 / 6 * elapsed);
	}
	// Look down
	if (glfwGetKey(mWindow, GLFW_KEY_S) == GLFW_PRESS) 
	{
		RotateCamera(0, -3.14 / 6 * elapsed);
	}
	// Strafe right
	if (glfwGetKey(mWindow, GLFW_KEY_D) == GLFW_PRESS) 
	{
		RotateCamera( 3.14 / 6 * elapsed, 0);
	}
	// Strafe left
	if (glfwGetKey(mWindow, GLFW_KEY_A) == GLFW_PRESS) 
	{
		RotateCamera(  -3.14 / 6 * elapsed, 0);
	}

	// Center camera
	if (glfwGetKey(mWindow, GLFW_KEY_C) == GLFW_PRESS) 
	{
		CRenderingManager::Instance()->GetCamera()->Center();
	}

	// Throw a pokeball.
	if (glfwGetKey(mWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		if (glfwGetTime() - mLastPokeballThrown > PokeballCooldown)
			ThrowPokeball();
	}

}

/**
 * The update call for the entire game.
 */
void CPokenstein3DGame::Update()
{
	if (mFirstCycle)
	{
		mPreviousTime = glfwGetTime();
		
	}

	
	double currentTime = glfwGetTime();


	double elapsed = currentTime - mPreviousTime;
	
	mPreviousTime = currentTime;

	//
	// If it's been 4 seconds since last spawn,
	// spawn a new pokemon.
	//
	if (currentTime - mLastPokemonSpawn > 4.0)
	{
		SpawnPokemon();
	}
	
	for (std::shared_ptr<CGameComponent> component : mComponents)
	{
		component->Update(elapsed);
	}
	GetControls((float)elapsed);

	
	FinalizeRemove();
}


/**
 * Move the player along the z-x coordinate space
 * \param translate z-x coordinates to add to the player's position vector.
 */
void CPokenstein3DGame::MovePlayer(glm::vec2 translate)
{
	CCamera * camera = CRenderingManager::Instance()->GetCamera();
	glm::vec3 forward = camera->GetForward();
	glm::vec3 right = camera->GetRight();
	forward *= translate[0];
	right *= translate[1];
	glm::vec3 movementVec = forward + right;
	mPlayer->Translate(forward + right);
	CRenderingManager::Instance()->MoveCamera(mPlayer->GetPosition() + glm::vec3(0.0f, -2.0f, 0.0f));

}



/**
 * Rotate the camera
 * \param h Pitch angle to rotate it in radians.
 * \param v Yaw angle to rotate it in radians
 */
void CPokenstein3DGame::RotateCamera(double h, double v)
{
	CCamera * camera = CRenderingManager::Instance()->GetCamera();
	camera->PitchRight(h);
	camera->PitchUp(v);
}

/** Add a game component to the game 
 * \param component The component to add. */
void CPokenstein3DGame::AddComponent(std::shared_ptr<CGameComponent> component)
{
	mComponents.push_back(component);
	component->SetGame(this);
}



/**
 * Add a Drawable component to the game.
 * \param component Component to add.
 */
void CPokenstein3DGame::AddComponent(std::shared_ptr<CDrawableGameComponent> component)
{
	mComponents.push_back(component);
	component->SetGame(this);
	CRenderingManager::Instance()->AddDrawable(component);
}


/**
 * Remove a component from the game
 * \param component Pointer to thecomponent to be removed.
 */
void CPokenstein3DGame::RemoveComponent(CGameComponent * component)
{
	for (std::vector<std::shared_ptr<CGameComponent> > ::iterator iter = mComponents.begin();
		iter != mComponents.end();
		++iter)
	{
		if (iter->get() == component)
		{
			mToRemove.push_back(*iter);
		}
	}
}


/**
 * Have a game component visitor visit all the game components.
 * \param visitor Visitor that needs to visit the game.
 */
void CPokenstein3DGame::AcceptVisitor(CGameComponentVisitor * visitor)
{
	for (std::shared_ptr<CGameComponent> component : mComponents)
	{
		component->AcceptVisitor(visitor);
	}
}



/**
 * Remove all the components that need to be removed. 
 * Call after iterating through all components in update.
 */
void CPokenstein3DGame::FinalizeRemove()
{
	for (std::shared_ptr<CGameComponent> component: mToRemove)
	{
		std::vector < std::shared_ptr<CGameComponent> >::iterator iter  = 
			find(mComponents.begin(), mComponents.end(), component);
		if (iter != mComponents.end())
		{
			mComponents.erase(iter);
		}
	}
	mToRemove.clear();
}


/**
 * Throws a pokeball. The pokeball will spawn in 
 * the direction of the camera, and move in the direction
 * the camera was facing the moment it spawned.
 */
void CPokenstein3DGame::ThrowPokeball()
{
	CCamera  * camera = CRenderingManager::Instance()->GetCamera();
	std::shared_ptr<CDrawableGameComponent> pokeball = std::make_shared<CPokeball>(camera->GetPosition(), camera->GetDirection(), this);
	
	AddComponent(pokeball);
	mLastPokeballThrown = glfwGetTime();

}



/**
 * Spawns a pokemon into the game.
 */
void CPokenstein3DGame::SpawnPokemon()
{
	// Seed for pseudo random generation.
	int seed = rand() % (8 - 1 + 1) + 1;
	std::shared_ptr<CDrawableGameComponent> pokemon;
	int choice = rand() % (3 - 1 + 1) + 1;
	glm::vec3 pos = glm::vec3(
		seed * 5,
		-3,
		(-1 + (2 * (seed % 2)) * (10 + 20 % (seed + 3))));
	switch (choice)
	{
	case 1:
		pokemon = std::make_shared<CBulbasaur>(pos);
		break;
	case 2:
		pokemon = std::make_shared<CCharmander>(pos);
		break;
	case 3:
		pokemon = std::make_shared<CBlastoise>(pos);
		break;
	default:
		break;
	}
	AddComponent(pokemon);
	pokemon->Initialize();
	mLastPokemonSpawn = glfwGetTime();
}
