/**
 * \file GameComponentVisitor.h
 *
 * \author Samantha Oldenburg
 *
 * A class that visits game components. Abstract.
 */

#pragma once
class CGameComponent;
class CDrawableGameComponent;
class CPhysicalGameComponent;
class CGameComponentVisitor;
class CPokemon;
class CSurface;


/**
 * A class that visits game components. Abstract.
 */
class CGameComponentVisitor
{
public:
	CGameComponentVisitor();
	virtual ~CGameComponentVisitor();
	
	virtual void VisitGameComponent(CGameComponent * component);

	virtual void VisitPokemon(CPokemon * pokemon);

	/** Visit a surface. 
	 * \param surface Surface to visit */
	virtual void VisitSurface(CSurface * surface) = 0;
};

