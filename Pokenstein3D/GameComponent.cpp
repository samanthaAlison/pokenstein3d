/**
 * \file GameComponent.cpp
 *
 * \author Samantha Oldenburg
 */

#include "GameComponent.h"
#include "GameComponentVisitor.h"
#include "Pokenstein3DGame.h"

/**
 * Constructor.
 */
CGameComponent::CGameComponent()
{
}


/**
 * Destructor.
 */
CGameComponent::~CGameComponent()
{
}


/**
 * Initialize the object post-construction.
 */
void CGameComponent::Initialize()
{
}


/**
 * Accept a game component visitor.
 * \param visitor Visitor to accept.
 */
void CGameComponent::AcceptVisitor(CGameComponentVisitor * visitor)
{
	visitor->VisitGameComponent(this);
}
/**
* Removes this component from the game. */
void CGameComponent::Remove()
{
	if (mGame != nullptr)
		mGame->RemoveComponent(this);
}
