/**
 * \file Bulbasaur.h
 *
 * \author Samantha Oldenburg
 *
 * A type of pokemon.
 */

#pragma once
#include "Pokemon.h"
/**
 * A type of pokemon.
 */
class CBulbasaur :
	public CPokemon
{
public:
	CBulbasaur(glm::vec3 pos);
	virtual ~CBulbasaur();

	virtual void LoadTexture() override;
};

