/**
 * \file GameComponentVisitor.cpp
 *
 * \author Samantha Oldenburg
 */

#include "GameComponentVisitor.h"



/**
 * Constructor.
 */
CGameComponentVisitor::CGameComponentVisitor()
{
}


/**
 * Destructor.
 */
CGameComponentVisitor::~CGameComponentVisitor()
{
}

/**
 * Visit a game component 
 * \param component The component to visit.
 */
void CGameComponentVisitor::VisitGameComponent(CGameComponent * component)
{
}

/**
* Visit a pokemon
* \param pokemon The pokemon to visit.
 */
void CGameComponentVisitor::VisitPokemon(CPokemon * pokemon)
{
}

