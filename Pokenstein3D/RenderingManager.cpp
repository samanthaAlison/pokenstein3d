/**
 * \file RenderingManager.cpp
 *
 * \author Samantha Oldenburg
 */

#include "RenderingManager.h"
#include <sstream>
#include <fstream>
#include <string>
#include "DrawableGameComponent.h"
#include "Rectangle.h"
#include "Billboard.h"
#include "TextureBoard.h"
#include "RoomFactory.h"
#include "Positionable.h"
/// Aspect Ratio
const float FoV = 45.0f;

/// Perspective Matrix of Renderer
const glm::mat4x4 ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.7f, 10000.0f);

CRenderingManager * CRenderingManager::_instance = nullptr;

CCamera CRenderingManager::mCamera;

GLFWwindow * CRenderingManager::mWindow;


std::vector<std::shared_ptr<CDrawableGameComponent > > CRenderingManager::mDrawables;

/**
 * Constructor.
 */
CRenderingManager::CRenderingManager()
{
}


/**
 * Returns the instance of the singleton
 * \returns An instance of the singleton
 */
CRenderingManager * CRenderingManager::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new CRenderingManager();
	}
	return _instance;
}

/**
 * Destructor.
 */
CRenderingManager::~CRenderingManager()
{
}

/**
 * Initialize this post-construction.
 * \param window Window to assign to this renderer.
 * \returns 0 if successful.
 */
int CRenderingManager::Initialize(GLFWwindow * window)
{
	mWindow = window;


	// Dark blue background
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	
	return 0;
}


/**
 * Draw all the drawable components.
 */
void CRenderingManager::Draw()
{
	// Clear screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	
	// Get camera view. 
	glm::mat4 view = mCamera.GetView();
	
	for (std::shared_ptr<CDrawableGameComponent>  drawable : mDrawables)
	{
		drawable->Draw(view, ProjectionMatrix);
	}
	glfwSwapBuffers(mWindow);
	glfwPollEvents();

	
}


//
// This function is from opengl-tutorial.org and is used under 
// the  WTF Public License.
//

/**
 * Create a shader program from shader files
 * \param vertex_file_path Filename of the vertex shader.
 * \param fragment_file_path Filename of the fragment shader.
 * \returns Location of the new shader program
 */
GLuint CRenderingManager::LoadShaders(const char * vertex_file_path, const char * fragment_file_path) {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::string Line = "";
		while (getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}
	else {
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::string Line = "";
		while (getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;


	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}



	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}



	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}


	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}


/**
 * Load drawable game components into the rendering manager during initialization.
 */
void CRenderingManager::Load()
{
	CRoomFactory roomFactory;
	std::shared_ptr<CDrawableGameComponent> room = roomFactory.Create(100.0f, 100.0f, 100.f);

	AddDrawable(room);

	for (std::shared_ptr<CDrawableGameComponent> drawable : mDrawables)
	{
		drawable->Initialize();
	}

}

/**
 * Translate the camera.
 * \param translation Vector to add to the camera's position.
 */
void CRenderingManager::MoveCamera(glm::vec3 translation)
{
	mCamera.SetPosition(translation);
}


/**
 * Add a drawable into the rendering manager.
 * \param drawable The drawable to add
 */
void CRenderingManager::AddDrawable(std::shared_ptr<CDrawableGameComponent > drawable)
{
	mDrawables.push_back(drawable);
}





/**
 * Removes a drawable from the rendering manager if it exists.
 * This removal does not need to be delayed provided that all
 * removal calls are done in update and none are while drawing.
 * \param drawable Pointer to the drawable to remove, if it is found.
 */
void CRenderingManager::RemoveDrawable(CDrawableGameComponent * drawable)
{
	for (std::vector<std::shared_ptr<CDrawableGameComponent> >::iterator iter = mDrawables.begin();
		iter != mDrawables.end();
		++iter)
	{
		if (iter->get() == drawable)
		{
			mDrawables.erase(iter);
			break;
		}
	}
}

