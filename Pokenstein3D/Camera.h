/**
 * \file Camera.h
 *
 * \author Samantha Oldenburg
 *
 * The camera that the game is viewed through.
 */

#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>

#include "Positionable.h"


/**
 * The camera that the game is viewed through.
 */
class CCamera : public CPositionable
{
public:
	CCamera();
	virtual ~CCamera();

	glm::mat4 GetView();
	/** Get the horizontal angle of the camera in radians. 
	 * \returns The horizontal angle. */
	double GetHorizontalAngle() { return mHorizontalAngle;  }

	/** Get the vertical angle of the camera in radians.
	* \returns The vertical angle. */
	double GetVerticalAngle() { return mVerticalAngle; }

	void PitchUp(double theta);
	/** Pitch the camera down  
	 * \param theta Angle to pitch in radians. */
	void PitchDown(double theta) { mVerticalAngle -= theta; }
	void PitchRight(double theta); 

	/** Pitch the camera left (yaw)
	* \param theta Angle to pitch in radians. */
	void PitchLeft(double theta) { mHorizontalAngle -= theta; }

	void Center();

	glm::vec3 GetForward();
	glm::vec3 GetRight();

	virtual void UpdateMatrix();
	glm::vec3 GetDirection();
private:
	/// Horizontal angle of the camera's rotation in radians
	double mHorizontalAngle = 0.0;
	/// Vertical angle of the camera's rotation in radians.
	double mVerticalAngle = 0.0;
	
	/// The up direction of the camera, and therfore the screen.
	glm::vec3 mUp = glm::vec3(0, 1, 0);
};

