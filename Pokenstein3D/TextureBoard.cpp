/**
 * \file TextureBoard.cpp
 *
 * \author Samantha Oldenburg
 */


#include "TextureBoard.h"
#include <iostream>

/// The vertex shader used by textureboards.
const std::string VertexShader("Shaders/TextureboardTransform.vertexshader");

/// The fragment shader used by textureboards.
const std::string FragmentShader("Shaders/Textureboard.fragmentshader");

//#define TEXTURE_LOAD_ERROR 0

using namespace std;


/// Numbers in comments represent quadrants on cartesian graph
const float Rectangle_UV_Coordinates[] = {
	1.0f, 0.0f, //2
	1.0f, 1.0f, //3
	0.0f, 1.0f, //4
	0.0f, 1.0f, //4
	0.0f, 0.0f,  //1
	1.0f, 0.0f, //2	

	};


//
// This function was taken from https://en.wikibooks.org/wiki/OpenGL_Programming/Intermediate/Textures
// and is sharable under the CC-Share alike: https://creativecommons.org/licenses/by-sa/3.0/
// no changes were made other than adding documentation to the function.
//

/**
 * Load a raw texture. Raw textures are files of unsigned chars that 
 * represent an 8-bit color value.
 * \param filename Name of the image file
 * \param width Width of the image
 * \param height Height of the image.
 * \returns Location of the new texture.
 */
GLuint raw_texture_load(const char *filename, int width, int height)
{
	GLuint texture;
	unsigned char *data;
	FILE *file;

	// open texture data
	fopen_s(&file, filename, "rb");
	if (file == NULL) 
		return 0;

	// allocate buffer
	data = (unsigned char*)malloc(width * height * 4);

	// read texture data
	fread(data, width * height * 4, 1, file);
	fclose(file);

	// allocate a texture name
	glGenTextures(1, &texture);

	// select our current texture
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	// select modulate to mix texture with color for shading
	//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// texture should tile
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// build our texture mipmaps
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);

	// free buffer
	free(data);

	return texture;
}


/**
 * Constructor.
 * \param w Width of the rectangle the texture will be drawn on.
 * \param h Height of the rectangle the texture will be drawn on.
 * \param pos Position of the rectangle the texture will be drawn on.
 */
CTextureBoard::CTextureBoard(float w, float h, glm::vec3 pos) : CBillboard(w, h, pos)
{

	LoadShaders(VertexShader.c_str(), FragmentShader.c_str());
}

/**
 * Destructor.
 */
CTextureBoard::~CTextureBoard()
{
	glDeleteBuffers(1, &mUVBuffer);
	glDeleteTextures(1, &mTextureSamplerID);

}



/**
 * Draws this textureboard, then disble the UV coordinates. 
 * The rest of the function is the same as a billboard.
 * \param view The view matrix of the camera.
 * \param perspective The perspective matrix of the camera.
 */
void CTextureBoard::Draw(glm::mat4 view, glm::mat4 perspective)
{
	CBillboard::Draw(view, perspective);
	glDisableVertexAttribArray(mVertexUVID);

}


/**
 * Initializes this object post-construction.
 */
void CTextureBoard::Initialize()
{
	CBillboard::Initialize();
	const GLuint programID = GetProgramID();
	mVertexUVID = glGetAttribLocation(programID, "vertexUV");

	glGenBuffers(1, &mUVBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mUVBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Rectangle_UV_Coordinates), Rectangle_UV_Coordinates, GL_STATIC_DRAW);
	

}


/**
 * Overrides the billboard's load color function. 
 * Loads a texture instead.
 */
void CTextureBoard::LoadColor()
{
	
	LoadTexture();
	
}

/**
 * Prepares this object to be drawn. Enables the texture UV 
 * coordinates buffer in order to draw the texture.
 */
void CTextureBoard::PrepareDraw()
{

	CBillboard::PrepareDraw();
	glEnableVertexAttribArray(mVertexUVID);
	glBindBuffer(GL_ARRAY_BUFFER, mUVBuffer);
	glVertexAttribPointer(
		mVertexUVID,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
}

/** Overrides the PrepareColor of the billboard. *
 * Prepares the texture and the uv coordinates to be drawn. */
void CTextureBoard::PrepareColor()
{

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glUniform1i(mTextureSamplerID, 0);
}


/**
 * Loads a texture from a file and sets it has a texture for the 
 * textureboard.
 * \param filename Filename of the image
 * \param width Width of the image.
 * \param height Height of the image
 */
void CTextureBoard::SetTexture(const char * filename, int width, int height)
{
	mTexture = raw_texture_load(filename, width, height);
	const GLuint programID = GetProgramID();
	mTextureSamplerID = glGetUniformLocation(programID, "myTextureSampler");

}
 