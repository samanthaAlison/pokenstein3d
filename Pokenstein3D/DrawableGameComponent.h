#pragma once
#include "PhysicalGameComponent.h"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include <glm\glm.hpp>
#include  <vector>
#include <memory>


/**
 * A game component that can be drawn.
 *
 * DrawableGameComponents are drawn by the RenderingManager.
 */
class CDrawableGameComponent : public CPhysicalGameComponent
{
public:
	CDrawableGameComponent(glm::vec3 size);
	virtual ~CDrawableGameComponent();

	virtual void Draw(glm::mat4 view, glm::mat4 perspective);

	virtual void Update(double elapsed) override;

	void AddChild(std::shared_ptr<CDrawableGameComponent> child);


	/** Set the parent component of this.
	 * \param parent The new parent of this component. */
	void SetParent(CDrawableGameComponent * parent) { mParent = parent; }

	virtual void Remove() override;
protected:

	void LoadShaders(const char * vertexShader, const char * fragmentShader);
	
	/** Gets the shader program of this component 
	 * \returns A reference to the program */
	GLuint & GetProgramID() { return mProgramID; }

private:
	
	void RemoveChild(CDrawableGameComponent * child);
	
	void FinalizeRemove();

	
	/// Contains the id for the vertex and fragment information 
	GLuint mProgramID;

	/// Contains the children of this component
	std::vector<std::shared_ptr< CDrawableGameComponent> > mChildren;
	
	/// The parent of the drawable game component.
	/// If this is nullptr, it has no parents.
	CDrawableGameComponent * mParent = nullptr;
	
	/// Contains children to remove during the next update call.
	std::vector<std::shared_ptr<CGameComponent> > mToRemove;
};

