/**
 * \file Rectangle.h
 *
 * \author Samantha Oldenburg
 *
 * A 2D rectangle that can be drawn in the 3D game space.
 */

#pragma once

#include <glm\glm.hpp>
#include "DrawableGameComponent.h"
#include <vector>



/**
 * A 2D rectangle that can be drawn in the 3D game space.
 */
class CRectangle :
	public CDrawableGameComponent 
{
public:
	CRectangle(glm::vec3 pos, glm::vec3 scale);
	CRectangle(float w, float h, glm::vec3 pos);
	virtual ~CRectangle();
	virtual void Initialize() override;
	virtual void Draw(glm::mat4 view, glm::mat4 perspective);
	
	/** Set the color of the object 
	 * \param color New color of the object */
	virtual void SetColor(glm::vec4 color) { mColor = color; }
	
	
	virtual void LoadColor();



protected:
	/** Get the ID of the color input in the fragment shader 
	 * \returns The color ID .*/
	GLuint & GetColorID() { return mColorID; }

	/** Get the ID of the matrix uniform in the vertex shader
	* \returns The matrix ID .*/
	GLuint & GetMatrixID() { return mMatrixID; }

	/** Get the ID of the vertex input in the vertex shader
	* \returns The vertex ID .*/
	GLuint & GetVertexPositionID()  { return mVertexPositionID; }
	
	/** Get the reference to the vertex buffer of this rectangle
	* \returns The vertex buffer's reference .*/
	GLuint & GetVertexBuffer() { return mVertexbuffer; }

	void LoadVertices();
	
	void DoDraw();
	
	/** Get the color value that will be used in this rectangle 
	 * \returns the RGBA value of the color */
	glm::vec4 GetColor() const { return mColor; }
	
	/** Sets the vertex position ID in the shader *
	 * \param vID New vertex position ID */
	void SetVertexPositionID(GLuint vID) { mVertexPositionID = vID; }


	virtual void PrepareDraw();

	virtual void PrepareColor();
private:
	/// Height of this rectangle.
	float mHeight;
	/// Width of this rectangle.
	float mWidth;

	/// Keeps track of the buffer for this vertex.
	GLuint mVertexbuffer;

	/// Keeps track of the vertex attribute in the shader.
	GLuint mVertexPositionID;

	/// Keeps track of the color attribute in this shader.
	GLuint mColorID;
	/// Keeps track of the matrix uniform in the shader.
	GLuint mMatrixID; 
	/// The color used to draw this rectangle.
	glm::vec4 mColor = glm::vec4(1.0, 0.0, 0.0, 1.0);

	/// A container of vertices of the rectangle.
	std::vector<glm::vec3> mVertices;

};

