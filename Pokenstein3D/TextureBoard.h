/**
 * \file TextureBoard.h
 *
 * \author Samantha Oldenburg
 *
 * A type of billboard that draws a texture instead of a solid object. 
 */

#pragma once
#include <GL/glew.h>
#include "Billboard.h"

/**
 * A type of billboard that draws a texture instead of a solid object.
 */
class CTextureBoard :
	public CBillboard
{
public:
	CTextureBoard(float w, float h, glm::vec3 pos);
	virtual ~CTextureBoard();
	virtual void Draw(glm::mat4 view, glm::mat4 perspective);
	virtual void Initialize() override;
protected:
	virtual void LoadColor() override;
	virtual void PrepareDraw() override;
	
	/** Loads a texture. Overrides LoadColor for textureboards. */
	virtual void LoadTexture() {};

	virtual void PrepareColor() override;

	void SetTexture(const char * filename, int width, int height);
private:
	/// Location of the texture attribute in the shader.
	GLuint mTextureSamplerID;
	/// Location of the texture used to draw this.
	GLuint mTexture;

	/// The ID for the UV coordinates in teh shaders
	GLuint mVertexUVID;
	/// The buffer for the vertices' texture UV coordiantes.
	GLuint mUVBuffer;
	
};

