/**
 * \file PhysicalGameComponent.cpp
 *
 * \author Samantha Oldenburg
 */

#include "PhysicalGameComponent.h"
#include "BoundingBox.h"

/**
 * Constructor.
 * \param size Size of the object as a vector
 */
CPhysicalGameComponent::CPhysicalGameComponent(glm::vec3 size) : 
	CPositionable(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f)),
	mBoundingBox(new CBoundingBox(this, size))
{
}

/**
 * Destructor.
 */
CPhysicalGameComponent::~CPhysicalGameComponent()
{
	delete mBoundingBox;
}



/**
 * Perform a hit test on this component. Identifies
 * if a point is within the area of this components 
 * bounding box
 * \param point The location we are to determine is in the 
 * component.
 * \returns True if the point is within the bounding box 
 * of the component.
 */
bool CPhysicalGameComponent::HitTest(glm::vec3 point)
{
	return mBoundingBox->HitTest(point);
}
