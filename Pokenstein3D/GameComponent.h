/**
 * \file GameComponent.h
 *
 * \author Samantha Oldenburg
 *
 * A component of our game. 
 */

#pragma once

class CGameComponentVisitor;
class CPokenstein3DGame;

/**
 * A component of our game. Components can have many functions.
 * They are updated each frame.
 */
class CGameComponent
{
public:
	CGameComponent();
	virtual ~CGameComponent();
	
	virtual void Initialize();
	/** Update the component 
	 * \param elapsed Time since last update */
	virtual void Update(double elapsed) {};

	virtual void AcceptVisitor(CGameComponentVisitor * visitor);
	
	
	virtual void Remove();

	/** Sets the pointer to the game.
	 * \param game Pointer to the new game that owns this. */
	void SetGame(CPokenstein3DGame * game) { mGame = game; }

protected:
	/** Get the game that this component runs in 
	 * \returns The pointer to the game. */
	CPokenstein3DGame * GetGame() { return mGame; }

private:
	/// Pointer to the games.
	CPokenstein3DGame * mGame = nullptr;

};

