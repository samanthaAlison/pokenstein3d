#pragma once
#include "Rectangle.h"

/**
 * A flat surface in our game, like wall or a floor.
 */
class CSurface :
	public CRectangle
{
public:

	CSurface(float w, float h, glm::vec3 pos);
	virtual ~CSurface();
	
	virtual void AcceptVisitor(CGameComponentVisitor * visitor) override;
	virtual void Draw(glm::mat4 view, glm::mat4 perspective) override;

};

