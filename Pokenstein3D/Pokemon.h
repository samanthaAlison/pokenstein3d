/**
 * \file Pokemon.h
 *
 * \author Samantha Oldenburg
 *
 * A pokemon that can be drawn in the game. 
 *
 * Pokemon are texture boards that move to different position in the
 * game space.
 */

#pragma once
//#define _COLLISION_DEMO

#include "TextureBoard.h"
/**
 * A pokemon that can be drawn in the game. 
 */
class CPokemon : public CTextureBoard
{
public:
	CPokemon(glm::vec3 pos);
	virtual ~CPokemon();

	virtual void Update(double elapsed);
private:
	/// Location the pokemon will move towards.
	glm::vec3 mDestination;
	/// Speed the pokemon moves towards its destination.
	float mSpeed = 1.0;

	void SetDestination();


	virtual void AcceptVisitor(CGameComponentVisitor * visitor) override;
	virtual void Draw(glm::mat4 view, glm::mat4 perspective) override;

};

