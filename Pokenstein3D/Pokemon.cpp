#include "Pokemon.h"
#include "GameComponentVisitor.h"
#include "BoundingBox.h"

/**
 * Constructor.
 * \param pos Position of the pokemon.
 */
CPokemon::CPokemon(glm::vec3 pos) : CTextureBoard(3.0f, 3.0f, pos)
{
	SetDestination();
}

/**
 * Destructor
 */
CPokemon::~CPokemon()
{
}

/**
 * Update the pokemon
 * \param elapsed Time since last update call
 */
void CPokemon::Update(double elapsed)
{
	glm::vec3 direction = mDestination - GetPosition(); 
	float length = glm::sqrt(direction.x * direction.x + direction.z * direction.z);
	if (length < 2.0f)
	{
		SetDestination();
	}
	else
	{
		direction = glm::normalize(direction) * (mSpeed * (float)elapsed);
		SetPosition(GetPosition() + direction);
	}

}
/**
 * Make the pokemon move to a destination 
 */
void CPokemon::SetDestination()
{

	float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 15));
	float z = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 15));
	float negx = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	float negz = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	if (negx >= 0.5)
		x = -x;
	if (negz >= 0.5)
		z = -z;
	mDestination = GetPosition() + glm::vec3(x, 0.0f, z);

}


/**
* Accept a game component visitor as a pokemon.
* \param visitor Visitor to accept.
 */
void CPokemon::AcceptVisitor(CGameComponentVisitor * visitor)
{
	visitor->VisitPokemon(this);
}



/**
 * Draws the pokemon
 * \param view The camera's view matrix
 * \param perspective The camera's perspective matrix
 */
void CPokemon::Draw(glm::mat4 view, glm::mat4 perspective)
{
	CTextureBoard::Draw(view, perspective);
#ifdef _COLLISION_DEMO
	GetBoundingBox()->DrawBox(perspective * view * GetTransformMatrix());
#endif // _COLLISION_DEMO
}