/**
 * \file Pokeball.h
 *
 * \author Samantha Oldenburg
 *
 * A textureboard that can be thrown by the player to catch pokemon.
 *
 * Uses the visitor pattern to check if it has hit a pokemon.
 */

#pragma once
#include "TextureBoard.h"
#include "GameComponentVisitor.h"
class CPokenstein3DGame;
/**
 * A textureboard that can be thrown by the player to catch pokemon.
 */
class CPokeball :
	public CTextureBoard, public CGameComponentVisitor
{
public:
	CPokeball(glm::vec3 pos, glm::vec3 direction, CPokenstein3DGame * game);
	virtual ~CPokeball();

	virtual void VisitPokemon(CPokemon * pokemon) override;
	virtual void VisitSurface(CSurface * surface) override;
	
	void SetDirection(glm::vec3 direction);
	virtual void Update(double elapsed) override;

	virtual void LoadTexture() override;

private:

	/// The direction our pokeball will be traveling in.
	glm::vec3 mDirection;

	/// Pointer to the game object.
	CPokenstein3DGame * mGame;
};

