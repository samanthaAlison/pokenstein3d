/**
 * \file RoomFactory.h
 *
 * \author Samantha Oldenburg
 *
 * A factory that creates the room we play the game in.
 */

#pragma once
#include <glm\glm.hpp>
#include <vector>
#include <memory>
class CDrawableGameComponent;
/**
 * A factory that creates the room we play the game in.
 */
class CRoomFactory 
{
public:
	CRoomFactory();
	virtual ~CRoomFactory();

	virtual std::shared_ptr<CDrawableGameComponent> Create();
	virtual std::shared_ptr<CDrawableGameComponent> Create(float w, float h, float d);

private:
};

