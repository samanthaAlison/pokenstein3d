#pragma once
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include <glm\glm.hpp>
#include <vector>
#include "DrawableGameComponent.h"

class CPhysicalGameComponent;

/**
 * A box that represents the volume an object takes
 * up in the game space. If object B is in Object A's bounding
 * box, object B has collided with object A.
 */
class CBoundingBox 
{
public:
	
	CBoundingBox(CPhysicalGameComponent * component, glm::vec3 scale);
	virtual ~CBoundingBox();

    void Initialize();
	bool HitTest(glm::vec3 point);

	void DrawBox(glm::mat4 mvp);

private:
	
	
	/// Vertices used for visual debugging. 
	std::vector<glm::vec3> mVertices;

	/// The component that this represents the volume of.
	CPhysicalGameComponent * mComponent;

	/// The corner  with the maxiumum x-y-z values of the bounding box.
	glm::vec3 mMaxPos;
	/// The corner  with the minimum x-y-z values of the bounding box.
	glm::vec3 mMinPos;


	

	/**
	 * \cond This code is used to draw the bounding box as if it were a cube
	 * only used for debugging purpose */
	GLuint mProgramID;
	GLuint mVertexbuffer;
	GLuint mVertexPositionID;
	GLuint mColorID;
	GLuint mMatrixID;
	glm::vec4 mColor = glm::vec4(1.0f, 1.0f, 1.0f, 5.0f);
	/**
	 * \endcond 
	 */

};

