/**
 * \file Pokeball.cpp
 *
 * \author Samantha Oldenburg
 */

#include "Pokeball.h"
#include "Pokemon.h"
#include "Surface.h"
#include "Pokenstein3DGame.h"

 /// Speed of the pokeball as it travels.
const float PokeBallSpeed(6.0f);
/// Image file of the pokeball.
const char * PokeballFileName("images/pokeballpng.data");
/// Width of the pokeball image.
const int PokeballWidth(50);
/// Height of the pokeball image.
const int PokeballeHeight(50);

/**
 * Constructor.
 * \param pos The position the pokeball spawns at.
 * \param direction The direction the pokeball will move in
 * \param game Pointer to the game object
 */
CPokeball::CPokeball(glm::vec3 pos, glm::vec3 direction, CPokenstein3DGame * game) : CTextureBoard(0.5f, 0.5f, pos), mDirection(direction), mGame(game)
{
	Initialize();
	LoadTexture();

}


/**
 * Destructor.
 */
CPokeball::~CPokeball()
{
}

/**
 * Visits a pokemon to see if it hits it.
 * \param pokemon Pokemone to visit
 */
void CPokeball::VisitPokemon(CPokemon * pokemon)
{
	if (pokemon->HitTest(GetPosition()))
	{
		pokemon->Remove();
		Remove();

	}
}
/** 
 * Check to see if the pokeball has hit a surface.
 * \param surface The surface we are visiting*/
void CPokeball::VisitSurface(CSurface * surface)
{
	if (surface->HitTest(GetPosition()))
	{
		surface->SetColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	}
}


/**
 * Set the direction the pokeball will move in.
 * \param direction The new direction of the pokeball's movement.
 */
void CPokeball::SetDirection(glm::vec3 direction)
{
	mDirection = direction;
}
/** Update this pokeball. 
 * have it check for collisions.
 * \param elapsed Time elapsed since last update call. */
void CPokeball::Update(double elapsed)
{
	Translate(mDirection * PokeBallSpeed * (float) elapsed);
	mGame->AcceptVisitor(this);
}

/**
 * Loads the texture for this item. Calls the SetTexture function to load the 
 * texture.
 */
void CPokeball::LoadTexture()
{
	SetTexture(PokeballFileName, PokeballWidth, PokeballeHeight);
}
