#include "Billboard.h"


/// Vertex shader to use for this
const std::string VertexShader("Shaders/BillboardTransform.vertexshader");
/// Fragment shader to use for this
const std::string FragmentShader("Shaders/SingleColor.fragmentshader");


/**
 * Constructor.
 * \param w Width of the billboard.
 * \param h Height of the billboard.
 * \param pos Position of the billboard.
 */
CBillboard::CBillboard(float w, float h,  glm::vec3 pos) : CRectangle(w, h, pos)
{
	LoadShaders(VertexShader.c_str(), FragmentShader.c_str());
	
}


/**
 * Destructor.
 */
CBillboard::~CBillboard()
{
}

/**
 * Initialize this object post-construction.
 */
void CBillboard::Initialize()
{
	LoadVertices();
	const GLuint programID = GetProgramID();

	mModelViewID = glGetUniformLocation(programID, "MView");
	mProjectionID = glGetUniformLocation(programID, "Projection");



	glUseProgram(programID);

	LoadColor();

	SetVertexPositionID(glGetAttribLocation(programID, "vertexPosition_modelspace"));
}

/**
 * Draw this object.
 * \param view Camera's view matrix.
 * \param perspective Camera's perspective matrix.
 */
void CBillboard::Draw(glm::mat4 view, glm::mat4 perspective)
{
	glm::mat4 MV = view * GetTransformMatrix();

	const GLuint programID = GetProgramID();

	

	glUseProgram(programID);
	glUniformMatrix4fv(mModelViewID, 1, GL_FALSE, &MV[0][0]);
	glUniformMatrix4fv(mProjectionID, 1, GL_FALSE, &perspective[0][0]);
	
	PrepareDraw();
	

	DoDraw();


}
