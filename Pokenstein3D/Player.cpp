#include "Player.h"



/**
 * Constructor.
 */
CPlayer::CPlayer() : CPhysicalGameComponent(glm::vec3(1.0f, 3.0f, 1.0f))
{
}


/**
 * Destructor.
 */
CPlayer::~CPlayer()
{
}

/**
 * Moves the player forward, based on the camera's forward direction
 * \param translate Distance to move in all 3 directions
 * \param forward The camera's forward direction (where it's pointing,
 * disregarding rotations in the x-axis (pitch).
 */
void CPlayer::Move(glm::vec3 translate, glm::vec3 forward)
{

	//translate.x = translate.z * forward.z
	SetPosition(GetPosition() + (forward * translate));
	

}
