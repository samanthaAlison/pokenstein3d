/**
 * \file PhysicalGameComponent.h
 *
 * \author Samantha Oldenburg
 *
 * A game component that takes up space in the game world. 
 */

#pragma once
#include "GameComponent.h"
#include "Positionable.h"
class CBoundingBox;
/**
 * A game component that takes up space in the game world. 
 *
 * They can be interacted with by other physical game components
 * and have bounding boxes that keep track of the space they currently
 * take up in the game world.
 */
class CPhysicalGameComponent : public CGameComponent, public CPositionable
{
public:
	CPhysicalGameComponent(glm::vec3 size);
	~CPhysicalGameComponent();

	virtual bool HitTest(glm::vec3 point);

	

protected:

	/** Gets the bouding box of this component 
	 * \returns A pointer to the bounding box. */
	CBoundingBox * GetBoundingBox() { return mBoundingBox; }

private:
	/// Bounding box of the component. Keeps track of the space it takes up.
	CBoundingBox * mBoundingBox;
};

