#pragma once

#include <GL\glew.h>
#define GLFW_INCLUDE_GLEXT
#include <GLFW/glfw3.h>
#include <glm\glm.hpp>
#include "Player.h"


#include <vector>
#include <memory>
class CGameComponent;
class CRenderingManager;
class CPokemon;
class CDrawableGameComponent;
class CGameComponentVisitor;


/**
 * The game itself. Holds all the components and runs the 
 * update->draw cycle.
 */
class CPokenstein3DGame
{
public:
	CPokenstein3DGame();
	~CPokenstein3DGame();

	int Start();
	


	void GetControls(float elapsed);
	void Update();

	void MovePlayer(glm::vec2 translate);
	void RotateCamera(double h, double v);

	void AddComponent(std::shared_ptr<CGameComponent> component);
	void AddComponent(std::shared_ptr<CDrawableGameComponent> component);

	void RemoveComponent(CGameComponent * component);


	void AcceptVisitor(CGameComponentVisitor * visitor);

	void FinalizeRemove();
private:
	/// Pointer to the window containing the game
	GLFWwindow * mWindow = nullptr;
	/// Time last frame was drawn at. 
	double mPreviousTime = 0.0;
	
	/// A player object.
	std::shared_ptr<CPlayer> mPlayer;

	/// True if this is the first Update->Draw cycle.
	bool mFirstCycle = true;

	/// Shared pointer to a pokemon
	std::shared_ptr<CPokemon> mPokemon;

	/// Contains all the game components currently in the game.
	std::vector < std::shared_ptr<CGameComponent> > mComponents;

	/// Game components to remove during the next update call.
	std::vector<std::shared_ptr<CGameComponent> > mToRemove;

	void ThrowPokeball();

	void SpawnPokemon();
	/// Time last pokemon was spawned in.
	double mLastPokemonSpawn = 0.0;
	/// Time last pokeball was thrown at.
	double mLastPokeballThrown = 0.0;
};

