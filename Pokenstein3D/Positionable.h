/**
 * \file Positionable.h
 *
 * \author Samantha Oldenburg
 *
 * Represents an object that can be moved, rotated, and scaled.
 *
 * Also provides functions to do so.
 */

#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>
/**
 * Represents an object that can be moved, rotated, and scaled.
 */
class CPositionable
{
public:
	CPositionable();
	CPositionable(glm::vec3 p, glm::quat r = glm::quat(glm::vec3(0.0f, 0.0f, 1.0f)), glm::vec3 s = glm::vec3(1.0f, 1.0f, 1.0f));
	CPositionable(glm::mat4 matrix);
	~CPositionable();

	
	virtual void UpdateMatrix();
	glm::vec3 GetPosition();
	/** Gets the rotation of the object 
	 * \returns rotation as a quaternion. */
	glm::quat GetRotation() { return mRotation; }

	/** Gets the transform matrix of the object *
	 * \returns The transform matrix of the object. */
	glm::mat4 GetTransformMatrix() { return mTransformMatrix; }

	void SetPosition(glm::vec3 pos);

	void Rotate(glm::vec3 rotation);


	void Rotate(float theta, glm::vec3 axis);

	void FaceAt(double h, double v);

	void SetRotation(glm::quat q);
	glm::vec3 GetRotationVector();


	void Translate(glm::vec3 translate);

private:
	/// The horizontal rotation of the object, radians.
	double mHorizontalAngle = 0.0;
	/// The vertical rotation of the object, radians.
	double mVerticalAngle = 0.0;
	/// Position of the object in the game space
	glm::vec3 mPosition;

	/// Rotation of the object represented as a quaternion.
	glm::quat mRotation;
	/// Scale of the object.
	glm::vec3 mScale;
	/// Skew of the object.
	glm::vec3 mSkew;
	/// Perspective of the object.
	glm::vec4 mPerspective;


	/// Matrix that represents all the rotational, positional, 
	/// and sizing properties of the object. Multiplying a 
	/// vertex by this matrix will apply the position, rotation,
	/// and scale to it.
	glm::mat4x4 mTransformMatrix;
};

