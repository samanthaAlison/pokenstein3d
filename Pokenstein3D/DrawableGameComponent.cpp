/**
 * \file DrawableGameComponent.cpp
 *
 * \author Samantha Oldenburg
 */

/**
 * \file DrawableGameComponent.cpp
 *
 * \author Samantha Oldenburg
 */

#include "DrawableGameComponent.h"
#include "RenderingManager.h"




/**
 * Constructor. 
 * \param size Size of the drawable game component.
 */
CDrawableGameComponent::CDrawableGameComponent(glm::vec3 size) : CPhysicalGameComponent(size)
{
}


/**
 * Destructor.
 */
CDrawableGameComponent::~CDrawableGameComponent()
{
	
	glDeleteProgram(mProgramID);

}

/**
 * Draw the children of this object.
 * \param view The camera's view matrix
 * \param perspective The camera's perspective matrix.
 */
void CDrawableGameComponent::Draw(glm::mat4 view, glm::mat4 perspective)
{
	for (std::shared_ptr<CDrawableGameComponent> child : mChildren)
	{
		child->Draw(view, perspective);
	}
}


/**
 * Updates this component. Removes any children that 
 * are queued to be removed.
 * \param elapsed Time elapsed since last update call.
 */
void CDrawableGameComponent::Update(double elapsed)
{
	CGameComponent::Update(elapsed);
	FinalizeRemove();
}


/**
 * Add a child to this component
 * \param child The child to add.
 */
void CDrawableGameComponent::AddChild(std::shared_ptr<CDrawableGameComponent> child)
{
	mChildren.push_back(child);
	child->SetParent(this);
}


/**
 * Remove this component from the game. 
 * 
 * Drawable game components must be also removed
 * from their parent's container and from
 * from the rendering manager.
 */
void CDrawableGameComponent::Remove()
{
	CGameComponent::Remove();
	if (mParent != nullptr)
	{
		mParent->RemoveChild(this);
	}
	CRenderingManager::Instance()->RemoveDrawable(this);
}

/** generates the shader program using the shader files 
 * \param vertexShader Filename of vertex shader.
 * \param fragmentShader Filename of the fragment shader */
void CDrawableGameComponent::LoadShaders(const char * vertexShader, const char * fragmentShader)
{
	mProgramID = CRenderingManager::LoadShaders(vertexShader, fragmentShader);
}



/**
 * Queues a child to be removed during the next update phase
 * \param child 
 */
void CDrawableGameComponent::RemoveChild(CDrawableGameComponent * child)
{
	for (std::vector<std::shared_ptr<CDrawableGameComponent> > ::iterator iter = mChildren.begin();
		iter != mChildren.end();
		++iter)
	{
		if (iter->get() == child)
		{
			mToRemove.push_back(*iter);
		}
	}
}


/**
 * Remove all the children that needed to be removed.
 */
void CDrawableGameComponent::FinalizeRemove()
{
	for (std::shared_ptr<CGameComponent> child : mToRemove)
	{
		std::vector < std::shared_ptr<CDrawableGameComponent> >::iterator iter =
			find(mChildren.begin(), mChildren.end(), child);
		if (iter != mChildren.end())
		{
			mChildren.erase(iter);
		}
	}
	mToRemove.clear();
}
