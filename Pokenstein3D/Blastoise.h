/**
 * \file Blastoise.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes a blaitoise. 
 * 
 * A blaistoise is a type of pokemon. 
 */

#pragma once
#include "Pokemon.h"

/**
 * Class that describes a blaitoise. 
 * 
 * A blaistoise is a type of pokemon.
 */
class CBlastoise :
	public CPokemon
{
public:
	CBlastoise(glm::vec3 pos);
	virtual ~CBlastoise();


protected:
	virtual void LoadTexture() override;
};

