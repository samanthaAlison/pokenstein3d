#include "Camera.h"
#include "Pokenstein3DGame.h"
using namespace glm;

/**
 * Constructor.
 */
CCamera::CCamera()
{
	SetPosition(glm::vec3(0, 4, 3));
	glm::quat pitch = glm::angleAxis((float)(mVerticalAngle), glm::vec3(1, 0, 0));
	glm::quat yaw = glm::angleAxis((float)(mHorizontalAngle), glm::vec3(0, 1, 0));
	glm::quat roll = glm::angleAxis((float)(0.0), glm::vec3(0, 0, 1));
	glm::quat q = pitch * yaw * roll;
	SetRotation(q);
}


/**
 * Destructor.
 */
CCamera::~CCamera()
{
}


/**
 * Get the view matrix of the camera.
 * \returns The view matrix of the camera.
 */
glm::mat4 CCamera::GetView()
{

	glm::vec3 direction = GetDirection();


	glm::vec3 up = glm::cross(GetRight(), direction);


	return glm::lookAt(GetPosition(), GetPosition() + direction, up);
}

/** Pitch the camera up
 * \param theta Angle to pitch in radians. 
 */
void CCamera::PitchUp(double theta)
{
	mVerticalAngle += theta; 
	
}

/** Pitch the camera right (yaw).
 * \param theta Angle to pitch in radians. 
 */
void CCamera::PitchRight(double theta)
{
	mHorizontalAngle += theta;
	
}

/**
 * Set the camera to face straight ahead at (0, 0, 1)
 */
void CCamera::Center()
{
	mVerticalAngle = 0.0f;
	mHorizontalAngle = 0.0f;
}


/**
 * Get the forward vector of the camera ( ignoring pitch/vertical angle).
 * \returns Forward vector.
 */
glm::vec3 CCamera::GetForward()
{
	return glm::vec3(
		glm::sin(mHorizontalAngle),
		0,
		glm::cos(mHorizontalAngle)); 
}


/**
 * Get the vector that represents the "to the right" of the camera.
 * \returns The right vector.
 */
glm::vec3 CCamera::GetRight()
{
	return glm::vec3(
		glm::sin(mHorizontalAngle + 3.14f / 2.0f),
		0,
		glm::cos(mHorizontalAngle + 3.14f / 2.0f)); 
}

/**
 * Updates the camera's transform matrix.
 */
void CCamera::UpdateMatrix()
{
	SetRotation(glm::quat(GetRotationVector()));
	CPositionable::UpdateMatrix();
}

/**
 * Gets the direction vector of the camera (where it is facing).
 * \returns The direction vector.
 */
glm::vec3 CCamera::GetDirection()
{
	return glm::vec3(
		cos(mVerticalAngle) * sin(mHorizontalAngle),
		sin(mVerticalAngle),
		cos(mVerticalAngle) * cos(mHorizontalAngle)
	);
}

