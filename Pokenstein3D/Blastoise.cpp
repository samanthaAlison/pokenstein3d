#include "Blastoise.h"
/// Filename of the image.
const char * BlastoiseFileName("images/blastoisepng.data");
/// Width of the image.
const int BlastoiseWidth(100);
/// Height of the image.
const int BlastoiseHeight(108);

/**
 * Constructor. 
 * \param pos Position to spawn the Blaistoise at.
 */
CBlastoise::CBlastoise(glm::vec3 pos) : CPokemon(pos)
{

}


/**
 * Destructor.
 */
CBlastoise::~CBlastoise()
{
}

/**
 * Loads the Blaistoise texture.
 */
void CBlastoise::LoadTexture()
{
	SetTexture(BlastoiseFileName, BlastoiseWidth, BlastoiseHeight);
}
