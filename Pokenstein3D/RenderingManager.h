/**
 * \file RenderingManager.h
 *
 * \author Samantha Oldenburg
 *
 * The rendering manager of the game.
 *
 * Draws all the drawable game components and keeps track of 
 * the camera. 
 */

#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <vector>
#include <memory>
#include "Camera.h"

class CDrawableGameComponent;
class CRectangle;
class CPositionable;
class CBillboard;
/**
 * The rendering manager of the game, responsible for drawing the game.
 */
class CRenderingManager
{
public:

	static CRenderingManager * Instance();

	virtual ~CRenderingManager();
	int Initialize(GLFWwindow * window);

	static void Draw();

	static GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);

	static void Load();


	static void  MoveCamera(glm::vec3 translation);

	/** Get the camera of the game
	 * \returns The camera of the game. */
	static CCamera * GetCamera() { return &mCamera; }

	static void AddDrawable(std::shared_ptr<CDrawableGameComponent > drawable);

	static void RemoveDrawable(CDrawableGameComponent * drawable);
	
	/** Get the position of the camera in the game space 
	 * \returns The position of the camera. */
	glm::vec3 GetCameraPosition() { return mCamera.GetPosition(); }

protected:
	CRenderingManager();


private:
	/// Instnace of the rendering manager.
	static CRenderingManager * _instance;
	/// The camera the game space is viewed through.
	static CCamera mCamera;

	/// The window the game is drawn on.
	static GLFWwindow * mWindow;

	/// Container for all the parentless drawables (drawables that are not
	/// children of other drawables).
	static std::vector<std::shared_ptr<CDrawableGameComponent > > mDrawables;
};

