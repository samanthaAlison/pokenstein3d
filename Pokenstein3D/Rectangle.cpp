/**
 * \file Rectangle.cpp
 *
 * \author Samantha Oldenburg
 */

#include "Rectangle.h"
#include "RenderingManager.h"
#include <string>
/// Vertex shader to use.
const std::string VertexShader("Shaders/SimpleTransform.vertexshader");
/// Fragment shader to use.
const std::string FragmentShader("Shaders/SingleColor.fragmentshader");

/**
 * Constructor.
 * \param pos Position of the rectangle.
 * \param scale Scale of the rectangle's bounding box
 */
CRectangle::CRectangle(glm::vec3 pos, glm::vec3 scale) : CDrawableGameComponent(scale)
{
	glm::mat4 transformMat = GetTransformMatrix();

	SetPosition(pos);
	SetRotation(glm::quat(0, 0, 0, 0));
	UpdateMatrix();
	glm::vec3 scaleTransform = glm::vec3(0.5f * scale.x, 0.5f * scale.y, 0.0f);
	mVertices = {
		glm::vec3(-1.0f, 1.0f, 0.0f),
		glm::vec3(-1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, 1.0f, 0.0f),
		glm::vec3(-1.0f, 1.0f, 0.0f) };


	for (std::vector<glm::vec3>::iterator iter = mVertices.begin(); iter != mVertices.end(); ++iter)
	{
		*iter = (*iter * scaleTransform);
	}
}


/**
 * Constructor.
 * \param w Width of rectangle
 * \param h Height of rectangle
 * \param pos Position of the rectangle.
 */
CRectangle::CRectangle(float w, float h, glm::vec3 pos) :
	mWidth(w), 
	mHeight(h), 
	CDrawableGameComponent(glm::vec3(w, h, (w > h) ? h : w)) 
{
	glm::mat4 transformMat = GetTransformMatrix();

	SetPosition(pos);
	SetRotation(glm::quat(0, 0, 0, 0));
	UpdateMatrix();
	glm::vec3 scaleTransform = glm::vec3(0.5f * w, 0.5f * h, 0.0f);
	mVertices = {
		glm::vec3(-1.0f, 1.0f, 0.0f),
		glm::vec3(-1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, -1.0f, 0.0f),
		glm::vec3(1.0f, 1.0f, 0.0f),
		glm::vec3(-1.0f, 1.0f, 0.0f) };


		for (std::vector<glm::vec3>::iterator iter = mVertices.begin();  iter != mVertices.end(); ++iter)
		{
			*iter = (*iter * scaleTransform);
		}
		
}
/** 
 * Destructor 
 */
CRectangle::~CRectangle()
{
	glDeleteBuffers(1, &mVertexbuffer);

}

/** Initialize this object. Load the shaders 
 * And generate buffers for vertices */
void CRectangle::Initialize()
{
	LoadShaders(VertexShader.c_str(), FragmentShader.c_str());
	

	const GLuint programID = GetProgramID();

	mMatrixID = glGetUniformLocation(programID, "MVP");
	mColorID = glGetUniformLocation(programID, "colorI");

	
	glUseProgram(programID);
	LoadColor();
	LoadVertices();
}



/**
 * Draw this object
 * \param view View matrix of the camera.
 * \param perspective Perspective matrix of the camera.
 */
void CRectangle::Draw(glm::mat4 view, glm::mat4 perspective)
{
	glm::mat4 transformMat = GetTransformMatrix();
	glm::mat4 MVP = perspective * view * GetTransformMatrix();

	

	const GLuint programID = GetProgramID();

	glUseProgram(programID);
	glUniformMatrix4fv(mMatrixID, 1, GL_FALSE, &MVP[0][0]);
	
	PrepareDraw();
	
	DoDraw();


	
}


/**
 * Loads the color ID information to the GLuint that keeps
 * track of that ID.
 */
void CRectangle::LoadColor()
{
	const GLuint programID = GetProgramID();
	mColorID = glGetUniformLocation(programID, "colorI");
}


/**
 * Creates the vertex buffer and loads the vertex information into that buffer.
 */
void CRectangle::LoadVertices()
{
	GLuint programID = GetProgramID();
	mVertexPositionID = glGetAttribLocation(programID, "vertexPosition_modelspace");

	glGenBuffers(1, &mVertexbuffer);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);

	glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(glm::vec3), &mVertices[0], GL_STATIC_DRAW);
}


/**
 * Do the actual drawing then disable the vertex buffer.
 */
void CRectangle::DoDraw()
{
	glDrawArrays(GL_TRIANGLES, (GLint)0, (GLsizei)mVertices.size()); // 3 indices starting at 0 -> 1 triangle

	glDisableVertexAttribArray(0);
}

/**
 * Load the color and vertex buffer to be drawn
 */
void CRectangle::PrepareDraw()
{
	PrepareColor();

	glEnableVertexAttribArray(mVertexPositionID);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);
	glVertexAttribPointer(
		mVertexPositionID,
		3,                            // size
		GL_FLOAT,                     // type
		GL_FALSE,                     // normalized?
		0,                            // stride
		(void*)0                      // array buffer offset
	);
}


/**
 * Load the color to be drawn.
 */
void CRectangle::PrepareColor()
{
	glUniform4f(mColorID, mColor.r, mColor.g, mColor.b, mColor.a);

}
